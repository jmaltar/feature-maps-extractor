import torch

def extractor(loader, net):
    device_cuda = torch.device("cuda:0")
    device_cpu = torch.device("cpu")

    net = net.to(device_cuda)
    net.eval()

    feature_vectors_database = []

    with torch.no_grad():
        for i, data in enumerate(loader):
            images, _ = data
            batch_size = images.size()[0]
            images = images.to(device_cuda)
            feature_vectors = net(images).view(batch_size, -1)
            feature_vectors_database.append(feature_vectors.to(device_cpu))

    return feature_vectors_database