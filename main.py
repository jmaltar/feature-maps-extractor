from data_loader import get_images_loader

from cnns.resnet18 import resnet18_layers
from cnns.resnet50 import resnet50_layers
from cnns.alexnet import alexnet_layers
from cnns.ConvOnlyNet import ConvOnlyNet

from feature_vectors_extractor import extractor
from AssociationMatrix import AssociationMatrix

import time

query_dataset_loader = get_images_loader(images_path="./data/bonn_example/query/", batch_size=1)
reference_dataset_loader = get_images_loader(images_path="./data/bonn_example/reference/", batch_size=1)


def get_association_matrix(net_layers, title, path):
    for nr_of_convs, layers in net_layers.items():
        net = ConvOnlyNet(layers)

        query_db = extractor(query_dataset_loader, net)
        reference_db = extractor(reference_dataset_loader, net)

        time_begin = time.time()

        am = AssociationMatrix()
        am.append_associations_columns(reference_db, query_db)

        time_end = time.time()

        am.save_to_file(path + title + "_{}.out".format(nr_of_convs))

        print(title + "_{}, passed: {}".format(str(nr_of_convs), str(time_end - time_begin)))



#get_association_matrix(alexnet_layers, "alexnet_places365", "./results/")

#get_association_matrix(resnet18_layers, "resnet18_places365", "./results/")

#get_association_matrix(resnet50_layers, "resnet50_places365", "./results")




