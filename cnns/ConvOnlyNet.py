import torch.nn as nn


class ConvOnlyNet(nn.Module):

    def __init__(self, layers):
        super(ConvOnlyNet, self).__init__()
        self.base = nn.Sequential(*layers)

    def forward(self, x):
        x = self.base(x)
        return x


