import torchvision
import torch

# load resnet50 (trained on places365)
resnet50 = torchvision.models.resnet50(num_classes=365)
checkpoint = torch.load("./models/resnet50_places365.pth.tar", map_location=lambda storage, loc: storage)
state_dict = {str.replace(k, 'module.', ''): v for k, v in checkpoint['state_dict'].items()}
resnet50.load_state_dict(state_dict)
# fetch all layers (excluding last average pooling & fully connected layer)
resnet50_all_layers = list(resnet50.children())[0:-2]

# resnet50 layers until (& including)
# 10th, 22th, 40th & 49th convolutional layer
resnet50_layers = {
    10: resnet50_all_layers[0:5],
    22: resnet50_all_layers[0:6],
    40: resnet50_all_layers[0:7],
    49: resnet50_all_layers[0:8]
}