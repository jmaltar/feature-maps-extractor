import torchvision
import torch

# load alexnet (trained on places365)
alexnet = torchvision.models.alexnet(num_classes=365)
checkpoint = torch.load("./models/alexnet_places365.pth.tar", map_location=lambda storage, loc: storage)
state_dict = {str.replace(k, 'module.', ''): v for k, v in checkpoint['state_dict'].items()}
alexnet.load_state_dict(state_dict)
alexnet_all_layers = list(alexnet.children())[0:-1][0]

# 3rd layer performs the best (trained on classical ImageNet)
# 4th layer performs the best (trained on Places365)
alexnet_layers = {
    1: alexnet_all_layers[0:3],
    2: alexnet_all_layers[0:6],
    3: alexnet_all_layers[0:8],
    4: alexnet_all_layers[0:10],
    5: alexnet_all_layers[0:13]
}