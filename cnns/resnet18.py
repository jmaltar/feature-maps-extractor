import torch
import torchvision

# load resnet18 (trained on places365)
resnet18 = torchvision.models.resnet18(num_classes=365)
checkpoint = torch.load("./models/resnet18_places365.pth.tar", map_location=lambda storage, loc: storage)
state_dict = {str.replace(k, 'module.', ''): v for k, v in checkpoint['state_dict'].items()}
resnet18.load_state_dict(state_dict)
# fetch all layers (excluding last average pooling & fully connected layer)
resnet18_all_layers = list(resnet18.children())[0:-2]

# resnet18 layers until (& including) 9th & 17th convolutional layer
resnet18_layers = {
    9: resnet18_all_layers[0:6],
    17: resnet18_all_layers[0:8]
}