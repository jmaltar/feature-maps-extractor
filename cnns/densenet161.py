import torch
import torchvision

# load densenet161 (trained on places365)
densenet161 = torchvision.models.densenet161(num_classes=365)
checkpoint = torch.load("../models/densenet161_places365.pth.tar", map_location=lambda storage, loc: storage)
state_dict = {str.replace(k, 'module.', ''): v for k, v in checkpoint['state_dict'].items()}
densenet161.load_state_dict(state_dict)
# fetch all layers (excluding last average pooling & fully connected layer)
densenet161_all_layers = list(densenet161.children())[0:-2]

