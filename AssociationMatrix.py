import torch
from torch.nn import CosineSimilarity

class AssociationMatrix:

    __slots__ = ["container"]

    def __init__(self):
        self.container = []

    def append_associations_column(self, reference_db, query_image):
        column = []
        device_cuda = torch.device("cuda:0")
        device_cpu = torch.device("cpu")


        query_image = query_image.to(device_cuda)
        cos_sim = CosineSimilarity()

        for reference_image in reference_db:
            reference_image = reference_image.to(device_cuda)
            column.append(cos_sim(reference_image, query_image).item())

        self.container.append(column)

    def append_associations_columns(self, reference_db, query_db):

        for query_image in query_db:
            self.append_associations_column(reference_db, query_image)

    def save_to_file(self, file_path):
        f = open(file_path, "w")

        nr_cols = self.container.__len__()
        nr_rows = self.container[0].__len__()

        f.write("{} {}\n".format(str(nr_cols), str(nr_rows)))

        for column in self.container:
            file_row = ""
            for value in column:
                file_row += str(value) + " "
            f.write(file_row)
        f.close()
