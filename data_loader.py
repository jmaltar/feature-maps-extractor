from torchvision import transforms, datasets
import torch.utils.data as data


def get_images_loader(images_path, batch_size=4):
    transform_images = transforms.Compose(
        [
            transforms.Resize(256),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            )
        ]
    )

    images_dataset = datasets.ImageFolder(root=images_path, transform=transform_images)
    images_dataset_loader = data.DataLoader(images_dataset, batch_size=batch_size, shuffle=False, num_workers=4)
    return images_dataset_loader
